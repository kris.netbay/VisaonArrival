var http = require('http'),
    https = require('https'),
    bodyParser = require('body-parser'),
    compression = require('compression'),
    log4js = require('log4js'),
    express = require('express'),
    helmet = require('helmet'),
    fs = require('fs');

var config = require('./config');
var globalConfig = config.getParam();
var schema = {};
var lib = {};

log4js.configure(globalConfig.LOG_CONFIG, { cwd: __dirname + "/logs/" });

lib.returnmessage = require('./module/returnmessage.js');
lib.transform = require('./module/transform.js');
lib.userprofile = require('./module/userprofile.js');
lib.date = require('./module/dateutil.js');
lib.config = globalConfig;
lib.log = log4js;

var app = express();


app.use(compression());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.disable('x-powered-by');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


app.use('/' + globalConfig.APPPATH, express.static(__dirname + '/app'));

app.set('port', process.env.PORT || globalConfig.PORT);

process.on('uncaughtException', function (err) {
    log4js.getLogger('error').fatal("UNCAUGHTEXCEPTION|" + err);
    console.log(err.stack);
});

process.on('exit', function () {
    console.log("Application Abort");
});

////////////////////
/// Security data https
///////////////////
var serverOption = {
    key: fs.readFileSync(globalConfig.PRIVATE_KEY_PATH, 'utf8'),
    cert: fs.readFileSync(globalConfig.CERT_PATH, 'utf8'),
    ca: fs.readFileSync(globalConfig.CA_PATH, 'utf8'),
};

if (globalConfig.isHttps) {
    var server = https.createServer(serverOption, app);
} else {
    var server = http.createServer(app);
}

server.timeout = globalConfig.HTTPREQUESTTIMEOUT;
server.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

