exports.json_success = function(data,msg) {
    return { "status": "OK", "message": msg, "data": data };
}

exports.json_error_msg = function(msg,data) {
    return { "status": "ERROR", "message": msg, "data": data };
}
