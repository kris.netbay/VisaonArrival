var userprofile = {};
var temptoken = "";

exports.setUserProfile = function (data) {
    userprofile = data;
}

exports.getUserProfile = function () {
    return userprofile;
}

exports.setToken = function (data) {
    temptoken = data;
}

exports.getToken = function () {
    return temptoken;
}