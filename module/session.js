exports.validatesession = function (param, url, db, lib, callback) {

    var setParam;

    if (url == '/service/global/GetNotification') {
        setParam = {}
    } else {
        setParam = {
            lasttick: new Date()
        }
    }

    var logMsg = {
        "/service/client/InsertRequest": "สร้างคำขอการขาย Invoice",
        "/service/client/GetMockDataMoney": "เข้าไปยังหน้าสร้างคำขอ",

        "/service/matching/AddMatching": "เพิ่มข้อมูล Matching",
        "/service/matching/DisableMatching": "ยกเลิกการใช้งานข้อมูล Matching",
        "/service/matching/EnableMatching": "เปิดการใช้งานข้อมูล Matching",

        "/service/user/ActiveUser": "เปิดการใช้งาน User",
        "/service/user/AddInternalUser": "สร้าง User ภายใน",
        "/service/user/AddUser": "สร้าง User ลูกค้า",
        "/service/user/ChangePassword": "เปลี่ยนรหัสผ่าน",
        "/service/user/DisableUser": "ปิดการใช้งาน User",
        "/service/user/FirsttimeActivateUser": "เข้าใช้งานครั้งแรก",
        "/service/user/ResetUserPassword": "รีเซ็ตรหัสผ่าน User",
        "/service/user/UpdateInternalUser": "แก้ใขข้อมูล User ภายใน",
        "/service/user/UpdateUser": "แก้ใขข้อมูล User"
    };

    var loglevel = 1;

    var logAdminList = [
        "/service/matching/AddMatching",
        "/service/matching/DisableMatching",
        "/service/matching/EnableMatching",

        "/service/user/ActiveUser",
        "/service/user/AddInternalUser",
        "/service/user/AddUser",
        "/service/user/DisableUser",
        "/service/user/FirsttimeActivateUser",
        "/service/user/ResetUserPassword",
        "/service/user/UpdateInternalUser",
        "/service/user/UpdateUser"

    ];

    if (logAdminList.indexOf(url) != -1) {
        loglevel = 2;
    }

    db.user.find({
        _id: param.uid,
        ucode: param.ucode,
        online: true
    }).exec(function (err, result) {
        if (err) {
            callback(err.errors, null);
        } else {
            if (result.length == 0) {
                callback("Dupplicate", null)
            } else {
                db.config.findOne({
                    "querykey": "sessiontime"
                }).exec(function (err, configresult) {
                    if (err) {
                        callback(err.errors, null);
                    } else {
                        var now = new Date().getTime();
                        var past = now - (1000 * 60 * parseFloat(configresult.data[0].value));
                        db.user.findOneAndUpdate({
                            _id: param.uid,
                            lasttick: {
                                $gt: new Date(past)
                            }
                        }, {
                                $set: setParam
                            }
                            , { new: true }).exec(function (err, newuserdata) {
                                if (err) {
                                    callback(err.errors, null);
                                }
                                else {
                                    if (newuserdata) {
                                        if (logMsg[url]) {
                                            var logData = new db.log({
                                                action: logMsg[url],
                                                by: newuserdata.firstname + " " + newuserdata.lastname,
                                                byid: newuserdata._id,
                                                timestamp: new Date(),
                                                loglevel: loglevel
                                            });

                                            logData.save(
                                                function (err, result) {
                                                    if (err) {
                                                        callback(err.errors, null);
                                                    } else {
                                                        callback(null, newuserdata)
                                                    }
                                                }
                                            );
                                        } else {
                                            callback(null, newuserdata)
                                        }


                                    } else {
                                      db.user.findOneAndUpdate({
                                          _id: param.uid
                                      }, {
                                              $set: {
                                                  online: false,
                                              }
                                          }).exec(function (err, result) {
                                              if (err) {
                                                callback("DB ERROR", null)
                                              }
                                              else {
                                                callback("Session expire", null)
                                              }
                                          });
                                    }
                                }
                            });
                    }
                });
            }
        }
    });
}
