exports.getDateStringFromDateObj = function (input, format, slash) {
    if (input == "" || input == undefined || input == null) {
        return null;
    } else {
        var date = new Date(input);
        var month = "";
        var day = "";
        if ((date.getMonth() + 1) > 9) {
            month = (date.getMonth() + 1);
        } else {
            month = "0" + (date.getMonth() + 1);
        }
        if (date.getDate() > 9) {
            day = date.getDate();
        } else {
            day = "0" + date.getDate();
        }
        if (format == "dmy") {
            return day + slash + month + slash
                + date.getFullYear();
        } else if (format == "ymd") {
            return date.getFullYear() + slash + month
                + slash + day;
        }
    }
};

exports.compareDate = function (date, comparedate) {
    var date1 = date.getTime();
    var date2 = comparedate.getTime();

    if (date1 >= date2) {
        return true;
    } else {
        return false;
    }
}

exports.dateDiff = function (date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.abs(Math.round(difference_ms / one_day));
}