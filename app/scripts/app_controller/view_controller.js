angular.module('homer').controller('ViewController', ViewController);

function ViewController(
    $scope,
    GlobalFunction,
    GlobalService,
    GlobalConfig
) {
    var THIS = this;

    THIS.viewid = GlobalService.get('viewid');

    var search = {
        _id: THIS.viewid
    }

    GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/service/global/view", search, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
            $scope.favorite = result.fav;
            $scope.data = result._source;
            $scope.id = result._id;
            $scope.dataForTheTree = formatObjData(result._source);
            console.log($scope.dataForTheTree);
        }
    });

    $scope.treeOptions = {
        nodeChildren: "child",
        dirSelectable: false,
        injectClasses: {
            ul: "a1",
            li: "a2",
            liSelected: "a7",
            iExpanded: "fa fa-minus text-danger",
            iCollapsed: "fa fa-plus  text-success",
            iLeaf: "a5",
            label: "a6",
            labelSelected: "a8"
        }
    }

    $scope.addfav = function () {
        GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/service/global/addfavorite",{_id:$scope.id,name:$scope.data.SchoolName,idcard:$scope.data.SchoolID}, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                $scope.favorite = result;
            }
        });
    }

    function formatObjData(data) {

        var result = [];
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                var value = data[key];

                if (Array.isArray(value)) {
                    var temp = {
                        key: key,
                        child: formatArrData(value),
                        type: "array"
                    };
                    result.push(temp);
                } else if (typeof value === 'object') {
                    var temp = {
                        key: key,
                        child: formatObjData(value),
                        type: "object"
                    };
                    result.push(temp);
                } else {
                    var temp = {
                        key: key,
                        value: value,
                        child: [],
                        type: "normal"
                    };
                    result.push(temp);
                }
            }
        }
        return result;
    }

    function formatArrData(data) {
        var result = [];
        data.forEach(function (element, index) {
            if (Array.isArray(element)) {
                var temp = {
                    key: "ลำดับ " + (index + 1),
                    child: formatArrData(element),
                    type: "array"
                };
                result.push(temp);
            } else if (typeof element === 'object') {
                var temp = {
                    key: "ลำดับ " + (index + 1),
                    child: formatObjData(element),
                    type: "object"
                };
                result.push(temp);
            } else {
                var temp = {
                    key: "ลำดับ " + (index + 1),
                    value: element,
                    child: [],
                    type: "normal"
                };
                result.push(temp);
            }
        });
        return result;
    }

    THIS.search = function () {
        sessionStorage.setItem('searchtext', THIS.searchText);
        GlobalFunction.changePage("search");
    }
}