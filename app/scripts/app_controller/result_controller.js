angular.module('Result', [

]).controller('ResultController', function (
    $scope,
    GlobalFunction,
    GlobalService,
    GlobalConfig,
    NgTableParams,
    $q
) {
    var THIS = this;

    THIS.init = true;
    // http://l-lin.github.io/angular-datatables/#/withAjax

    
    THIS.searchText = sessionStorage.getItem('searchtext');
    console.log(THIS.searchText);

    THIS.loadData = function () {
        sessionStorage.setItem('searchtext', THIS.searchText);
        var params = THIS.tableParams;
        var filter = params.filter();
        var sorting = params.sorting();
        var count = params.count();
        var page = params.page();
        var search = {
            sort: sorting,
            pagenum: page,
            pagecount: count,
            search: {
                searchText: THIS.searchText
            }
        }

        var deferred = $q.defer();

        GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/service/global/search", search, function (err, result) {
            if (err) {
                deferred.reject(result);
            } else {
                console.log(result);
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }

    THIS.tableParams = new NgTableParams({
        page: 1,
        count: 10
    }, {
            counts: [5, 10, 20, 50, 100],
            getData: function (params) {
                return THIS.loadData().then(function (data) {
                    $scope.maxscore = data.maxscore;
                    THIS.tableParams.total(data.totalItem);
                    console.log(THIS.tableParams);
                    if (THIS.tableParams.total() % THIS.tableParams.count() == 0 && THIS.tableParams.page() == (THIS.tableParams.total() / THIS.tableParams.count()) + 1 && THIS.tableParams.page() != 1) {
                        THIS.tableParams.page(THIS.tableParams.page() - 1);
                    }
                    return data.data;
                });
            }
        });


    THIS.search = function () {
        THIS.tableParams.page(1);
        THIS.tableParams.reload();
        THIS.tableParams.reloadPages();
    }

    THIS.view = function (params) {
        console.log(params);
        GlobalService.set('viewid', params._id);
        GlobalFunction.changePage("viewsearch");
    }

});