angular.module('Hist', [

]).controller('HistController', function (
    $scope,
    GlobalFunction,
    GlobalService,
    GlobalConfig,
    notify,
    NgTableParams,
    $q
) {

    var THIS = this;

    THIS.type = sessionStorage.getItem("viewhistparam");
    THIS.loadData = function () {
        var params = THIS.tableParams;
        var filter = params.filter();
        var sorting = params.sorting();
        var count = params.count();
        var page = params.page();
        var search = {
            sort: sorting,
            pagenum: page,
            pagecount: count,
            search: {
                type: sessionStorage.getItem("viewhistparam")
            }
        }

        var deferred = $q.defer();

        GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/service/global/searchhist", search, function (err, result) {
            if (err) {
                deferred.reject(result);
            } else {
                console.log(result);
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }

    THIS.tableParams = new NgTableParams({
        page: 1,
        count: 10
    }, {
            counts: [5, 10, 20, 50, 100],
            getData: function (params) {
                return THIS.loadData().then(function (data) {
                    THIS.tableParams.total(data.totalItem);
                    console.log(THIS.tableParams);
                    if (THIS.tableParams.total() % THIS.tableParams.count() == 0 && THIS.tableParams.page() == (THIS.tableParams.total() / THIS.tableParams.count()) + 1 && THIS.tableParams.page() != 1) {
                        THIS.tableParams.page(THIS.tableParams.page() - 1);
                    }
                    return data.data;
                });
            }
        });

    THIS.search = function () {
        sessionStorage.setItem('searchtext', THIS.searchText);
        GlobalFunction.changePage("search");
    }
    
});