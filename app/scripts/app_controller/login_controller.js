angular.module('Login', [

]).controller('LoginController', function (
    $scope,
    GlobalFunction,
    GlobalService,
    GlobalConfig,
    notify
) {

    $scope.login = function () {

        var credential = {
            username: $scope.username,
            password: $scope.password
        };
        $scope.loading = true;
        GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/public/login", credential, function (err, result) {
            $scope.loading = false;
            if (err) {
                notify({ message: result, classes: 'alert-warning', templateUrl: 'views/notification/notify.html' });
            } else {
                if ($scope.remember) {
                    localStorage.setItem('token', result.token);
                    localStorage.setItem('userdata', result.data);
                }else{
                    GlobalService.set('token', result.token);
                    GlobalService.set('userdata', result.data);
                }
                GlobalFunction.changePage("dashboard");
            }
        });
    }

});