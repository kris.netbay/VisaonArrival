angular.module('TouristView', [

]).controller('TouristViewController', function (
    $scope,
    GlobalFunction,
    GlobalService,
    GlobalConfig
) {

    $scope.token = localStorage.getItem('token');
    // console.log($scope.token);
    var getID = sessionStorage.getItem("tid"); 
    //console.log("ID : "+getID);
    var THIS = this;

    THIS.viewid = getID;

    var search = {
        tid: THIS.viewid
    }

    GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/public/tourist_detail",search, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            // console.log(result);
            $scope.data = result;
        }
    });

    $scope.goBack = function(){
        GlobalFunction.changePage("tourist_page");
    }
});