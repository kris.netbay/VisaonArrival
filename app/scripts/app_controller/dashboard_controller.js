angular.module('homer').controller('DashboardController', DashboardController);

function DashboardController(
    $scope,
    GlobalFunction,
    GlobalService,
    GlobalConfig
) {
    var THIS = this;

    $scope.now = new Date();
    THIS.search = function () {
        sessionStorage.setItem('searchtext', THIS.searchText);
        GlobalFunction.changePage("search");
    }

    $scope.chartUsersOptions = {
        series: {
            splines: {
                show: true,
                tension: 0.01,
                lineWidth: 1,
                fill: 0.4
            },
        },
        grid: {
            tickColor: "#f0f0f0",
            borderWidth: 1,
            borderColor: 'f0f0f0',
            color: '#6a6c6f'
        },
        xaxis: {
            tickDecimals: 0,

        },
        colors: ["#62cb31", "#efefef"],
    };

    GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/service/global/getStatistic", {}, function (err, result) {
        if (!err) {
            $scope.statistic = result;
            $scope.pageview = result[0].pageview;
            $scope.pageviewlog = result[0].pageviewlog;
            $scope.usercount = result[1].usercount;
            $scope.visit = result[2].visit;
            $scope.searchcount = result[3].searchcount;
            $scope.searchhist = result[3].search;
            $scope.searchcounttoday = result[3].searchcounttoday;
            $scope.profilecount = result[4].profilecount;
            $scope.favorite = result[5].favorite;
            $scope.lastsearch = $scope.searchhist.slice(0, 4);
            $scope.chartUsersData = [transformDataToGraph($scope.searchhist)];
            $scope.avgDay = calcAvgDay($scope.chartUsersData[0]);
        }
    });

    $scope.viewHistory = function (at) {
        sessionStorage.setItem("viewhistparam", at);
        GlobalFunction.changePage("viewhistory");
    }

    $scope.viewFav = function (params) {
        console.log(params);
        GlobalService.set('viewid', params);
        GlobalFunction.changePage("viewsearch");
    }

    $scope.removeFav = function (params) {

        var index  = $scope.favorite.indexOf(params);
        GlobalFunction.sendLoadData(GlobalConfig.endpoint + "/service/global/removeFav", params, function (err, result) {
            if (!err) { 
                $scope.favorite.splice(index,1);
            }
        });
    }

    $scope.searchHist = function (value) {
        sessionStorage.setItem('searchtext', value);
        GlobalFunction.changePage("search");
    }

    function transformDataToGraph(data) {
        var temp = {};
        var result = [];
        if (data.length > 0) {
            for (var index = 0; index < data.length; index++) {
                var element = data[index];
                var date = new Date(element.timestamp).getDate();
                if (temp[date] == undefined) {
                    temp[date] = 0;
                } else {
                    temp[date] = temp[date] + 1;
                }
            }

            console.log(temp);
            for (var key in temp) {
                if (temp.hasOwnProperty(key)) {
                    var element = temp[key];
                    var x = [];

                    x[0] = parseInt(key);
                    x[1] = element;
                    result.push(x);
                }
            }
            var tlength = 8
            if (result.length < tlength) {
                var lastdate = result[result.length - 1][0];
                var pushtime = tlength - result.length;

                for (var index = 1; index < pushtime; index++) {
                    var d = [];

                    d[0] = lastdate + index;
                    d[1] = 0;
                    result.push(d);
                }
            }
        }
        return result;
    }

    function calcAvgDay(params) {
        console.log(params);
        var count = 0;
        var sum = 0;
        params.forEach(function(element) {
            console.log(element);
            if(element[1] >0){
                count++;
                sum += element[1];
            }
        });
        console.log(count,sum);
        return sum/count;
    }
}