angular.module('homer').controller('TouristController', function (
    $scope,
    GlobalFunction,
    GlobalService,
    GlobalConfig,
    NgTableParams,
    notify,
    $q
) {

    $scope.token = localStorage.getItem('token');
    // console.log($scope.token);
            
    var THIS = this;
    
    THIS.loadData = function () {
        sessionStorage.setItem('searchtext', THIS.searchText);
        var params = THIS.tableParams;
        var filter = params.filter();
        var sorting = params.sorting();
        var count = params.count();
        var page = params.page();
        var search = {
            sort: sorting,
            pagenum: page,
            pagecount: count,
            search: filter
        }
        
        var deferred = $q.defer();
        GlobalFunction.sendLoadData(GlobalConfig.endpoint +"/public/tourist", search, function (err, result) {
            if (err) {
                deferred.reject(result);
            } else {
                //console.log(result);
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }
   
    THIS.tableParams = new NgTableParams({
        page: 1,
        count: 10
    }, {
            counts: [5, 25, 50, 100],
            getData: function (params) {
                return THIS.loadData().then(function (data) {
                    THIS.tableParams.total(data.totalItem);
                    //console.log(THIS.tableParams);
                    if (THIS.tableParams.total() % THIS.tableParams.count() == 0 && THIS.tableParams.page() == (THIS.tableParams.total() / THIS.tableParams.count()) + 1 && THIS.tableParams.page() != 1) {
                        THIS.tableParams.page(THIS.tableParams.page() - 1);
                    }
                    return data.data;
                });
            }
        });

    $scope.showDetail = function(item){
        //console.log("Show detail");
        //console.log(item);
        var id = item._id;
        sessionStorage.setItem("tid",id)
        GlobalFunction.changePage("tourist_view");
    }


});