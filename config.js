
var config = {
    "APPPATH": "app",
    "PORT": 9876,

    "HTTPREQUESTTIMEOUT": 120000,
    "LOG_CONFIG": {
        "appenders": [
            {
                "type": "dateFile",
                "filename": "out/service_out.log",
                "pattern": "-yyyy-MM-dd",
                "category": "service_out"
            },
            {
                "type": "dateFile",
                "filename": "in/service_in.log",
                "pattern": "-yyyy-MM-dd",
                "category": "service_in"
            },
            {
                "type": "dateFile",
                "filename": "other/other.log",
                "pattern": "-yyyy-MM-dd",
                "category": "other"
            },
            {
                "type": "dateFile",
                "filename": "interceptor/interceptor.log",
                "pattern": "-yyyy-MM-dd",
                "category": "interceptor"
            },
            {
                "type": "dateFile",
                "filename": "error/error.log",
                "pattern": "-yyyy-MM-dd",
                "category": "error"
            }
        ]
    },


    //HTTPS
    isHttps:true,
    PRIVATE_KEY_PATH:"./cert/private.key",
    CERT_PATH:"./cert/cert.crt",
    CA_PATH:"./cert/private.csr",

    SECRET_WEB_TOKEN:"bosdygboasygouydogiuaysbdgou"
}

exports.getParam = function() {
    return config;
}
